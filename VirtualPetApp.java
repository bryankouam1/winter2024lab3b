import java.util.Scanner;
public class VirtualPetApp{
	public static void main(String[]args){
		Scanner reader = new Scanner(System.in);
		Koala[] colony = new Koala[4];
		
		for(int i=0;i<colony.length;i++){
			colony[i] = new Koala();
			
			System.out.println("Enter gender: Female or Male");
			colony[i].gender = reader.nextLine();
			
			System.out.println("Enter age:");
			colony[i].age = Integer.parseInt(reader.nextLine());
			
			System.out.println("Enter weight:");
			colony[i].weight = Integer.parseInt(reader.nextLine());
			
		}
		
		System.out.println("The gender of the Koala: "+colony[colony.length-1].gender);
		System.out.println("The age of the Koala: "+colony[colony.length-1].age);
		System.out.println("The weight of the Koala: "+colony[colony.length-1].weight);
		
		colony[0].hoursOfSleep();
		colony[0].eatFood();
	}
	
	
	
}