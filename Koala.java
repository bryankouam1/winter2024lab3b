public class Koala{
	public String gender;
	public int age;
	public int weight;
	
	public void hoursOfSleep(){
		if(this.gender.equals("Female")){
			System.out.println("Koala can sleep up to 20 hours!");
		}
		else{
			System.out.println("Koala can sleep up to 18 hours!");
		}
		
	}
	
	public void eatFood(){
		if(this.weight < 15){
			System.out.println("Koala needs to eat more RIGHT NOW!!");
		}
		else if(this.weight >=15 && this.weight<29){
			System.out.println("Koala does not to change diet :)");
			
		}
		else{
			System.out.println("Koala is eating too much! Must reduce quantity of food intake.");
		}
	}
	
}